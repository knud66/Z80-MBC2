https://www.facebook.com/groups/Z80MBC2

https://hackaday.io/project/159973-z80-mbc2-a-4-ics-homebrew-z80-computer?fbclid=IwAR3q5R4nFGSwAScV45uBW-XrOhyMGjnVLcd3O-Au42vCJQa7Dgle92h3RyQ#menu-files

Modified version of S220718-R240620 
- added an EEPROM editor
- added I2C scan devices
- modified clock to calculate speed of CPU
- Moved functions above void setup{} and void loop{}
- Optimized SRAM

Everyone is free to use it. It's tested by me with Arduino 1.8.14 with 
 - PetitFS 0.03a library
 - avr-gcc version 10.2.0
 - avr-ar version 2.35 (GNU Binutils 2.35)
 - avr-libc version 2.0.0
 - MightyCore 2.0.5 ATmega32. https://github.com/MCUdude/MightyCore#how-to-install
With a little mod it should work with an ATmega1284. (Change OC2 to OC2A)
Also a lot of mods should work on the v20-MBC. (not tested)

20201017:
	- Updated the i2c (still need to optimize using int in place of byte)
	- Can recognize A110417 
	- Updated bootmenu showing what to boot
	- Clean out old remarked code
	- removed library LiquidCrystal_PCF8574
	- Updated the i2c to auto configure LCD
	- Added Lcd.... routines to sketch (No library needed)	
	- RTC not calculating correct DayOfWeek
	- RTC temperature still not correct 
